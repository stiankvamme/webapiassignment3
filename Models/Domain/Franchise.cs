﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment3WebApi.Models.Domain
{
    [Table("Franchise")]
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Desctiption { get; set; }
#nullable enable
        public ICollection<Movie>? Movies { get; set; }
    }
}
